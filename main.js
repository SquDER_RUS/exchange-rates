'use strict'

let date;
let startDate;
let endDate;
let usdTrueValues;
let eurTrueValues;
const select = document.getElementById('timeInterval');

interval();

function getRequestURL() {
  if (select.value === "selectYear") {
    endDate = moment().format('YYYY-MM-DD');
    startDate = moment().subtract(1, 'years').format('YYYY-MM-DD');
  }
  if (select.value === "selectMonth") {
    endDate = moment().format('YYYY-MM-DD');
    startDate = moment().subtract(1, 'months').format('YYYY-MM-DD');
  }
  if (select.value === "selectWeek") {
    endDate = moment().format('YYYY-MM-DD');
    startDate = moment().subtract(1, 'weeks').format('YYYY-MM-DD');
  }
}


function interval() {
  getRequestURL();
  async function requestData() {
    const result = await fetch(`https://api.exchangeratesapi.io/history?start_at=${startDate}&end_at=${endDate}&base=RUB`);
    const data = await result.json();
    const newData = Object.entries(data.rates).sort();
    date = Object.keys(data.rates).sort();
    let usdValues = [];
    let eurValues = [];
    usdTrueValues = [];
    eurTrueValues = [];
    for (let key in newData) {
      usdValues.push(newData[key]);
      eurValues.push(newData[key]);
    }
    usdValues.forEach((usdTrue) => {
      usdTrueValues.push(1 / usdTrue[1].USD)
    });
    eurValues.forEach((eurTrue) => {
      eurTrueValues.push(1 / eurTrue[1].EUR)
    });
  }
  async function chartFunc() {
    await requestData();

    Highcharts.chart('chart', {
      chart: {
        type: 'line'
      },
      title: {
        text: 'Exchange Russian Rouble Rates'
      },
      subtitle: {
        text: 'published by the European Central Bank'
      },
      xAxis: {
        categories: date
      },
      yAxis: {
        title: {
          text: 'Value'
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true,
            format: "{point.y:.2f}"
          },
          enableMouseTracking: true
        }
      },
      tooltip: {
        valueDecimals: 2,
        valueSuffix: ' RUB'
      },
      series: [{
        name: 'EUR',
        data: eurTrueValues
      }, {
        name: 'USD',
        data: usdTrueValues
      }]
    });
  }
  chartFunc();
}